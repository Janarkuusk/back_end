<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>
    <a class="bg-gray-500 text-white m-60" href="{{route('products.add')}}">Add new product</a>
    @foreach ($products as $product)
    <p>{{$product->name}}</p>
    <p>{{$product->description}}</p>
    <p>{{$product->price}}</p>
    <img src="{{$product->image}}" alt="product image">
    @endforeach
</x-app-layout>