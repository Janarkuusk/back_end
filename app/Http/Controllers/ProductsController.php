<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function index()
    {
        return view('products', ['title' => 'Products']);
    }
}
    